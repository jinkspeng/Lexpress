var http = require("http")
var url = require("url")
var fs = require("fs")


Express = {
	version: '0.0.1',
	responseCodes : {
	  100 : 'Continue',
	  101 : 'Switching Protocols',
	  200 : 'OK',
	  201 : 'Created',
	  202 : 'Accepted',
	  203 : 'Non-Authoritative Information',
	  204 : 'No Content',
	  205 : 'Reset Content',
	  206 : 'Partial Content',
	  300 : 'Multiple Choices',
	  301 : 'Moved Permanently',
	  302 : 'Moved Temporarily',
	  303 : 'See Other',
	  304 : 'Not Modified',
	  305 : 'Use Proxy',
	  400 : 'Bad Request',
	  401 : 'Unauthorized',
	  402 : 'Payment Required',
	  403 : 'Forbidden',
	  404 : 'Not Found',
	  405 : 'Method Not Allowed',
	  406 : 'Not Acceptable',
	  407 : 'Proxy Authentication Required',
	  408 : 'Request Time-out',
	  409 : 'Conflict',
	  410 : 'Gone',
	  411 : 'Length Required',
	  412 : 'Precondition Failed',
	  413 : 'Request Entity Too Large',
	  414 : 'Request-URI Too Large',
	  415 : 'Unsupported Media Type',
	  500 : 'Internal Server Error',
	  501 : 'Not Implemented',
	  502 : 'Bad Gateway',
	  503 : 'Service Unavailable',
	  504 : 'Gateway Time-out',
	  505 : 'HTTP Version not supported'
	},
	utilities: {},
	modules: [],
	routes: [],

	// --- Response

	response: {
		headers: {},
		cookie: {}
	},

	// --- Settings

	settings: {
		basepath : '/',
		defaultRoute: {
			callback: function() {  //在路径比对不成功时默认callback
				Express.status(404)
				return 'Page Not Found'
			}
		}

	},

	// --- Modules

	RedirectHelpers: {
		init:  function() {  //初始化
			Express.home = Express.settings.basepath
		},
		onRequest : {
		  'set back to referrer' : function() {
		    Express.back =
		      Express.header('Referer') ||
		        Express.header('Referrer')
		  }
		}
	},

	ContentLength: {
		onResponse: {
			'set content length': function() {
				Express.header('Content-Length', (Express.response.body || '').length)
			}
		}
	},

	DefaultContentType: {
		onRequest: {
			'set response type to text/html': function() {
				Express.header('Content-Type', 'text/html')
			}
		}
	},

	BodyDecoder: {
        onRequest: {
        	'parse urlencoded bodies': function() {  //如果是表单上传
        		switch(Express.header('Content-Type')) {
        			case 'application/x-www-form-urlencoded':  //decodeURIComponent解码
        			    Express.request.params = Express.parseParams(decodeURIComponent(Express.request.body))
        			    break
        			case 'application/json':
        			    Express.request.params = Express.jsonDecode(decodeURIComponent(Express.request.body))
        			    break
        			default:
        			    break
        		}
        	}
        }
	},

	MethodOverride: {
        onRequest: {
        	'set HTTP method to _method when present': function() {
        		var method
        		if (method = Express.param('_method'))
                    Express.request.method = method.toUpperCase()
        	}
        }
	},

	Logger: {
		onResponse: { //响应的时候(数据已经处理完)打印出
			'output log data': function() {
				console.log('"' + Express.request.method + Express.request.url +
				 ' HTTP/' + Express.request.httpVersion + '" - ' + Express.response.status)
			}
		}
	},

//-----------处理挂钩------------


	/**
	 * 使用hook
	 *
	 * @param {string} name
	 * @param {arguments} args
	 * @return {array}
	 * @api public
	 */
	hook: function(name, args) {
		var results = []
			for (var i = 0, len = this.modules.length; i < len; ++i)
				for (var key in this.modules[i][name])
				    results.push(this.modules[i][name][key].apply(this, this.toArray(arguments, 1)))
		return results
	},

	/**
	 * Invoke hook _name_ with _immutable_ and _args_, returning
	 * the _immutable_ variable once modified by each hook implementation.
	 * When implementing these hooks, the function must return a value.
	 *
	 * @param  {string} name
	 * @param  {mixed} immutable
	 * @param  {...} args
	 * @return {mixed}
	 * @api public
	 */
	hookImmutable: function(name, immutable, args) {
        for (var i = 0, len = this.modules.length; i < len; ++i)
        	for (var key in this.modules[i][name])
                immutable = this.modules[i][name][key].call(this.modules[i], immutable)
        return immutable
	},

	/**
	 * 添加module
	 *
	 * @param {hash} module
	 * @api public
	 */
	addModule: function(module, args) {
		Express.settings[module.name] = {}

		if ('init' in module)  //module有init
			module.init.apply(module, Express.toArray(arguments, 1))  //module有init则先触发

		if ('utilities' in module)  //module有utilities
			for (var name in module.utilities)
				Express.utilities[name] = module.utilities[name]

		if ('settings' in module) {  //module有name, settings
			if (!module.name) throw 'module name is required when using .settings'
			for (var name in module.settings)
			    Express.settings[module.name][name] = module.settings[name]
        }


		Express.modules.push(module)
	},


//-----------入口------------

	/**
	 * 开始
	 *
	 * @param {int} port
	 * @param {string} host
	 * @api public
	 */
	start: function(port, host) {
		this.server.listen(port || 3000, host, this.server.callback)
	},

	server: {
		/**
		 * 监听
		 *
		 * @param {int} port
		 * @param {string} host
		 * @param {function} callback
		 * @api private
		 */

		listen: function(port, host, callback) {
			http.createServer(callback).listen(port, host)
			console.log('Express listening to http://' + (host || '127.0.0.1') + ':' + port + '/')
		},

		/**
		 * 请求回调函数
		 *
		 * @param  {object} req
		 * @param  {object} res
		 * @api public
		 */
<<<<<<< HEAD
		callback: function(req, res) {
			var buffers = []
			req.on('data', function(chunk) {  //简单处理
			    buffers.push(chunk)
=======
		pathToRegexp: function(path) {
<<<<<<< HEAD
			if (path.constructor == RegExp) return path
=======
			if(path.constructor == RegExp) return path
>>>>>>> day02
			Express.regexpKeys = [] //当前处理路径的param key
			path = path.replace(/:(\w+)/g, function(_, key) {
				Express.regexpKeys.push(key)
				return '(.*?)'
>>>>>>> day04
			})
			req.on('end', function() {
				req.body = Buffer.concat(buffers).toString()  //保存body
				Express.home = Express.settings.basepath
				Express.response.body = null
				Express.response.status = 200
				Express.request = req //保存请求对

				Express.originalResponse = res  //保存原先的esponse
				req.path = Express.normalizePath(url.parse(req.url, true).pathname) //保存path
				req.params = []

				Express.hook('onRequest')

				try {
					Express.response.body = Express.callRouteFor(req)
				} catch(err) {
					Express.fail(err)
				}
				if (typeof Express.response.body == 'string') {
				  Express.server.finished()
				}

			})
		},

		finished: function(body) {
			if (body) Express.response.body = body
			Express.hook('onResponse')
		    Express.originalResponse.writeHead(Express.response.status, Express.response.headers)
		    Express.originalResponse.write(Express.response.body || '')
		    Express.originalResponse.end()
		}
	},

	/**
	 * '生成body失败'处理
	 *
	 * @param {object} err
	 * @api public
	 */
	fail: function(err) {
		this.hook('onFailure', err)  //错误处理时的hook
		this.header('Content-Type', 'text/plain')
		this.response.body = this.halt(err.name + ':' + err.message, 500)
	},

//-----------请求响应数据处理------------

	/**
	 * 查找保存的params
	 *
	 * @param  {string} key
	 * @param  {mixed} defaultValue
	 * @return  {mixed}
	 * @api public
	 */
	param: function(key, defaultValue) {
		return this.request.params && this.request.params[key] || defaultValue
	},

	/**
	 * 设置响应状态码
	 *
	 * @param  {int, string} value
	 * @api public
	 */
	status: function(value) {
		if (value == undefined) throw 'Express.status(): value must be passed'
		if (typeof value ==='number')
			return this.response.status = parseInt(value)
		for(var code in this.responseCodes)
			if (this.responseCodes[code].toLowerCase() == value.toLowerCase())
				return this.response.status = parseInt(code)
	},

	/**
	 * 获取或设置响应头
	 *
	 * @param {string} name
	 * @param {string} value
	 * @api public
	 */
	header: function(name, value) {
		name = name.toLowerCase()
		return value ? this.response.headers[name] = value : this.response.headers[name]
	},

	/**
	 * 对保存的cookie进行读取或设置
	 *
	 * @param {string} cookie
	 * @return {hash}
	 * @api public
	 */
	cookie: function(key, value) {
		if (typeof value === 'undefined') return this.request.cookie[key]
		return this.response.cookie[key] = value
	},

	/**
	 * 设置status, body
	 *
	 * @param {string} body
	 * @param {string, int} status
	 * @return {string}
	 * @api public
	 */
	halt: function(body, status) {
		this.status(status || 200)
		Express.server.finished(body)
	},

	/**
	 * 重定向
	 *
	 * @param {string} uri
	 * @return {string}
	 * @api public
	 */
	redirect: function(uri) {
		this.header('Location', uri)
		return this.halt('Moved Temporarily', 302)
	},

//-----------路径处理------------
	/**
	 * 生成处理不同请求的方法
	 *
	 * @param  {function} method
	 * @return {}
	 * @api public
	 */
	routeFunctionFor: function(method) {
		return function(path, options, callback) {
			if (path.constructor == String) path = Express.normalizePath(path)
			if (options.constructor == Function)  //增加options判断
				callback = options, options = {}
			var route = {
				path: Express.pathToRegexp(path),
				keys: Express.regexpKeys || [],
				method: method,
				options: options,
				callback: callback
			}
			Express.routes.push(route)
			Express.hook('onRouteAdded', route)
		}
	},

	/**
	 * 返回处理后的body
	 *
	 * @param {object} req
	 * @retrun {mixed}
	 * @api public
	 */
	callRouteFor: function(req) {
		var route = this.findRouteFor(req) || this.settings.defaultRoute

		if (route.keys) //url有格式/xx:key存在
			for (var i = 0, l = route.keys.length; i < l; ++i) {
				this.request.params[route.keys[i]] = Express.captures[i + 1] || null
			}
		var body = route.callback(Express.captures)
		if (typeof body !== 'string')
			throw "route `" + route.method.toUpperCase() + ' ' + route.path + "' must return a string"
		return body
	},

	/**
	 * 检查路径
	 *
	 * @param {object} req
	 * @param {object} req
	 * @return {object}
	 * @api public
	 */
	findRouteFor: function(req) {
		for (var i = 0, l = this.routes.length; i < l; ++i)
			if (this.routeMatches(this.routes[i], req))
				return this.routes[i]
	},

	/**
	 * 查找保存路径中是否有当前路径
	 *
	 * @param {object} req
	 * @return {object}
	 * @api public
	 */
	routeMatches: function(route, req) {
		var conditions = [Express.routeMethod, Express.routeProvides]
		for (var i = 0, len = conditions.length; i < len; ++i)
			if (!conditions[i](route, req))
				return false
		switch (route.path.constructor) {  //路径格式分为字符串和正则
			case String: return route.path == req.path
			case RegExp: return !!(Express.captures = req.path.match(route.path)) //捕捉正则处理
		}
	},

	/**
	 * 判断请求方法
	 *
	 * @param {hash} route
	 * @param {hash} req
	 * @return {boolean}
	 * @api private
	 */
	routeMethod: function(route, req) {
		return route.method.toUpperCase() == req.method
	},

	/**
	 * 判断保存的可选provide是否是请求头部包含的accept中
	 *
	 * @param {hash} route
	 * @param {hash} req
	 * @return {boolean}
	 * @api private
	 */
	routeProvides: function(route, req) {
		if (!route.options.provides) return true
		if (route.options.provides.constructor !== Array)
			route.options.provides = [route.options.provides]
		for (var i = 0, len = route.options.provides.length; i < len; ++i)
			if (req.headers.accept && req.headers.accept.match(route.options.provides[i]))
				return true
		return false
	},

//-----------工具函数------------
	/**
	 * 转换hash到array
	 *
	 * @param  {hash} hash
	 * @return {array}
	 * @api public
	 */
	hashToArray: function(hash) {
		var arry = [];
		for (var key in hash)
			array.push([key, hash[key]])
		return array;
	},

	/**
	 * 转换 array-like 到 into array
	 *
	 * @param  {object} arr
	 * @param  {int} offset
	 * @return {array}
	 * @api public
	 */
	toArray : function(arr, offset) {
	  return [].slice.call(arr, offset)
	},

	/**
	 * 转换array到hash
	 *
	 * @param  {array} array
	 * @return {hash}
	 * @api public
	 */
	arrayToHash: function(array) {
		var hash = {}
		for (var i = 0, l = array.length; i < l; ++i)
			hash[array[i][0].toLowerCase()] = array[i][1]
		return hash
	},

	/**
	 * 转义特殊的html字符
	 *
	 * @param  {string} html
	 * @return {string}
	 * @api public
	 */
	escape: function(html) {
		return html.replace(/&/g, '&amp;')
			   .replace(/"/g, '&quot;')
			   .replace(/</g, '&lt;')
			   .replace(/>/g, '&gt;')
	},

	/**
	 * 转义特殊的正则字符
	 *
	 * @param  {string} path
	 * @param  {string} chars
	 * @return {Type}
	 * @api public
	 */
	escapeRegexp: function(string, chars) {
		var specials = (chars || '/ . * + ? | ( ) [ ] { } \\').split(' ').join('|\\')
		return string.replace(new RegExp('(\\' + specials + ')', 'g'), '\\$1')
	},

	/**
	 * 路径字符串转化为正则格式
	 * 如 /user/:id/edit 将会转化为 \^/user/(.*?)\/edit\$/, id将会捕捉
	 *
	 * @param  {string} path
	 * @return {regexp}
	 * @api public
	 */
	pathToRegexp: function(path) {
		if (path.constructor == RegExp) return path
		Express.regexpKeys = [] //当前处理路径的param key
		path = path.replace(/:(\w+)/g, function(_, key) {
			Express.regexpKeys.push(key)
			return '(.*?)'
		})
		return new RegExp('^' + this.escapeRegexp(path, '/ [ ]') + '$', 'i')
	},

	/**
	 * 去除路径多余斜杠
	 *
	 * @param  {string} path
	 * @return {string}
	 * @api public
	 */
	normalizePath: function(path) {
		return path.replace(/^(\s|\/)*|(\s|\/)*$/g, '')
	},

	/**
	 * 解析参数:
	 * { user[info][name] : 'xx' } -> { user : { info : { name : 'xx' }}}
	 *
	 * @param {hash} route
	 * @param {hash} req
	 * @return {boolean}
	 * @api private
	 */
	parseNestedParams: function(params) {
		for (var key in params)
			if (parts = key.split('['))
				if (parts.length > 1)
				for (var i = 0, prop = params, len = parts.length; i < len; ++i) {
					var name = parts[i].replace(']', '')
					if (i == len - 1) {
						prop[name] = params[key]
						props = params
						delete params[key]
					} else {
						prop = prop[name] = prop[name] || {}
					}
				}
		return params
	},

	/**
	 * 解析cookie变成hash:
	 *
	 * @param {string} cookie
	 * @return {hash}
	 * @api public
	 */
	parseCookie: function(cookie) {
		var hash = {}
		if (!cookie) return hash
		var attrs = cookie.split(/\s*;\s*/)
		for (var i = 0; i < attrs.length; ++i)
			hash[attrs[i].split('=')[0]] = unescape(attrs[i].split('=')[1])
		return hash
	},


	/**
	 * JSON encode _object_
	 *
	 * @param  {mixed} object
	 * @return {string}
	 * @api public
	 */
	jsonEncode : function(object) {
	  return JSON.stringify(object)
	},

	/**
	 * JSON decode _string_.
	 *
	 * @param  {string} string
	 * @return {mixed}
	 * @api public
	 */
	 jsonDecode : function(string) {
	   return JSON.parse(string)
	 },

	 /**
	  * 返回路径分割后特定字段
	  *
	  * @param  {init} n
	  * @return {string}
	  * @api public
	  */
	 arg: function(n) {
	 	return this.request.path.split('/')[n]
	 }
}

// --- Routing API

get = Express.routeFunctionFor('get');  //保存get请求处理路径的方法
post = Express.routeFunctionFor('post');  //保存post请求处理路径的方法
del = Express.routeFunctionFor('delete');
put = Express.routeFunctionFor('put');
use = Express.addModule

// --- Core Modules


