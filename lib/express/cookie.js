Express.Cookie = {
	name: 'cookie',

	settings: {
		maxAge: 3600
	},

	utilities: {
		/**
		 * 对保存的cookie进行读取或设置
		 *
		 * @param {string} cookie
		 * @return {hash}
		 * @api public
		 */
		cookie: function(key, value) {
			return value ? Express.response.cookie[key] = value : Express.response.cookie[key]
		},
	},

	onRequest: {
		'parse cookie': function() {
			Express.request.cookie =
				Express.response.cookie =
					Express.parseCookie(Express.header('Cookie'))
		}
	},

	onResponse: {
		'set cookie': function() {
			var cookie = []
			for (var key in Express.response.cookie)
				if (Express.response.cookie.hasOwnProperty(key))
					cookie.push(key + '=' + Express.response.cookie[key])
			cookie.length && Express.header('Set-Cookie', cookie.join('; '))
		}
	}
}

